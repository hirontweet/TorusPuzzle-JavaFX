/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TorusUser;

import TorusCore.ITorusPuzzle;
import TorusComponent.Button;

/**
 *
 * @author Yamazaki Laboratory
 */
public class UserCode {
    
    /*
        ** 注意 **
        この部分のコードに変更を加えないでください
    */
    private ITorusPuzzle Torus = null;
    
    /*
        ** 注意 **
        この部分のコードに変更を加えないでください
    */
    public UserCode(ITorusPuzzle torus){ // この関数を「コンストラクタ」と呼びます
        Torus = torus;
    }
    
    /**
     * 自分のコードをここに書く
     */
    public void myCode(){
        
        
        for(int y = 0; y < 5; y++){
            for(int x = 0; x < 5; x++){
                Torus.makeBlock(x, x, y);
            }
            Torus.makeButton(y, 0, y+1);
            Torus.makeButton(y, y+1, 0);
        }
        
        Torus.display("HelloWorld");
        Torus.display("HelloWorld");
        Torus.display("HelloWorld");
        Torus.display("HelloWorld");
        Torus.display("HelloWorld");
        Torus.display("HelloWorld");
        
        
        
        
    }
    /*
        自分のコードはここまで
    */
    
    
}
