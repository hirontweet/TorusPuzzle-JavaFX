/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TorusGUI;

import TorusCore.TorusPuzzle;
import TorusUser.UserCode;
import javafx.application.Application;
import javafx.stage.Stage;


/**
 *
 * @author Yamazaki Laboratory
 */
public class TorusPuzzleGUI extends Application {
    
    /**
     * ウィンドウの大きさを指定
     */
    public static final int GUI_WIDTH = 800;
    public static final int GUI_HEIGHT = 600;
    
    @Override
    public void start(Stage primaryStage) {
        
        // ウィンドウのタイトルをセット
        primaryStage.setTitle("Torus Puzzle");
        
        // トーラスパズルのロジックにステージのインスタンスを渡す
        TorusPuzzle torusPuzzle = new TorusPuzzle(primaryStage);
        // ユーザがいじるトーラスパズルのインスタンスを渡す
        UserCode userCode = new UserCode(torusPuzzle);
        
        // ユーザーのコードを実行
        userCode.myCode();
        
        // ユーザーが加えた変更をprimaryStageを取得
        torusPuzzle.updateStage();
        
        // primaryStageを表示する
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // launch()を呼び出すとTorusPuzzleGUI.start()が呼び出される
        launch(args);
    }
    
}
