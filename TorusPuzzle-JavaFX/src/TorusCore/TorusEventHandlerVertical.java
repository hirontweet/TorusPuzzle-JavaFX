/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TorusCore;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 *
 * @author Yamazaki Laboratory
 * 
 * Buttonクラスが設定されたButtonコンポーネントに設定する垂直移動のイベントハンドラー
 * Button.MOVE_VERTICALが設定されているButtonに対して使う
 */
public class TorusEventHandlerVertical implements EventHandler<ActionEvent> {

    private int mIndex = 1;
    private TorusPuzzle mTorus;

    public TorusEventHandlerVertical(TorusPuzzle torus, int index) {
        mTorus = torus;
        mIndex = index;
    }

    @Override
    public void handle(ActionEvent event) {
        mTorus.moveVertically(mIndex);
    }

}
