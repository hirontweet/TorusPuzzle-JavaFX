/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TorusCore;

import TorusComponent.Block;
import TorusComponent.Button;
import TorusGUI.TorusPuzzleGUI;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 *
 * @author Yamazaki Laboratory
 */
public class TorusPuzzle implements ITorusPuzzle{
   
    // コンポーネントの表示個数
    public static final int MAX_COMPONENT_AMOUNT = 5;
    public static final int MAX_BLOCK_FOR_HORIZONTAL = MAX_COMPONENT_AMOUNT;
    public static final int MAX_BLOCK_FOR_VERTICAL = MAX_COMPONENT_AMOUNT;
    public static final int MAX_BUTTON_FOR_HORIZONTAL = MAX_COMPONENT_AMOUNT + 1;
    public static final int MAX_BUTTON_FOR_VERTICAL = MAX_COMPONENT_AMOUNT + 1;
    
    // GUI側で表示される見た目のサイズ
    public static final int PREFERED_GRID_SIZE = Math.min(TorusPuzzleGUI.GUI_HEIGHT, TorusPuzzleGUI.GUI_WIDTH) / (MAX_COMPONENT_AMOUNT + 1 + 1);
    public static final int MARGIN_SIZE = 5;
    public static final int BUTTON_SIZE = PREFERED_GRID_SIZE - 2 * MARGIN_SIZE;
    
    // コンポーネントを管理する配列
    private Button[] mButtonOnGridVertical;
    private Button[] mButtonOnGridHorizontal;
    private Block[][] mBlockOnGrid;
    
    // GUIのインスタンス管理メンバ変数
    private Stage mGUI;
    
    // display()呼び出しによる文字列の管理
    private StringBuffer mStringOutput;
    
    // トーラスパズルの有無
    private boolean mTorusExist = false;
    
    public TorusPuzzle(Stage stage){
        
        // GUI表示インスタンスの引き継ぎ
        mGUI = stage;
        
        // Block用の配列の初期化
        mBlockOnGrid = new Block[MAX_BLOCK_FOR_VERTICAL][MAX_BLOCK_FOR_HORIZONTAL];
        for(int y = 0; y < MAX_BLOCK_FOR_VERTICAL; y++){
            for(int x = 0; x < MAX_BLOCK_FOR_HORIZONTAL; x++){
                mBlockOnGrid[y][x] = null;
            }
        }
        
        // Button用の配列の初期化（垂直・水平）
        mButtonOnGridHorizontal = new Button[MAX_BUTTON_FOR_HORIZONTAL];
        mButtonOnGridVertical = new Button[MAX_BUTTON_FOR_VERTICAL];
        for(int i = 0; i < MAX_BUTTON_FOR_VERTICAL; i++){
            mButtonOnGridHorizontal[i] = null;
            mButtonOnGridVertical[i] = null;
        }
        
        // 文字列出力の初期化
        mStringOutput = new StringBuffer();
    }
    
    /**
     * 
     * @param userInput 表示する文字列を入力
     */
    public void display(int userInput){
        
        // 今はコンソールに表示されるように設定している
        System.out.println(userInput);
        
        mStringOutput.append(userInput);
        mStringOutput.append("\n");
    }

    /**
     * 
     * @param userInput 表示する文字列を入力
     */
    public void display(float userInput){
        
        // 今はコンソールに表示されるように設定している
        System.out.println(userInput);
        
        mStringOutput.append(userInput);
        mStringOutput.append("\n");
    }
    
    /**
     * 
     * @param userInput 表示する文字列を入力
     */
    public void display(double userInput){
        
        // 今はコンソールに表示されるように設定している
        System.out.println(userInput);
        
        mStringOutput.append(userInput);
        mStringOutput.append("\n");
    }
    
    /**
     * 
     * @param userInput 表示する文字列を入力
     */
    public void display(char userInput){
        
        // 今はコンソールに表示されるように設定している
        System.out.println(userInput);
        
        mStringOutput.append(userInput);
        mStringOutput.append("\n");
    }
    
    /**
     * 
     * @param userInput 表示する文字列を入力
     */
    public void display(String userInput){
        
        // 今はコンソールに表示されるように設定している
        System.out.println(userInput);
        
        mStringOutput.append(userInput);
        mStringOutput.append("\n");
    }
    
    /**
     * ブロックをステージに追加する
     * @param num ブロックに表示させる数字
     * @param coord_x ブロックのx座標
     * @param coord_y ブロックのy座標
     */
    public void makeBlock(int num, int coord_x, int coord_y){
        
        // makeBlockが呼ばれたら、トーラスパズルは存在する
        doTorusExists();
        
        Block newBlock = new Block(num);
        try{
            mBlockOnGrid[coord_y][coord_x] = newBlock;
        }catch(ArrayIndexOutOfBoundsException e){
            System.err.println("配列の要素番号がしきい値を超えています");
            newBlock = null;
        }
    }
    
    /**
     * ボタンをステージ上に追加する
     * @param direction ボタンの方向(VERTICAL, HORIZONTAL)
     * @param coord_x ボタンのx座標
     * @param coord_y ボタンのy座標
     */
    public void makeButton(int direction, int coord_x, int coord_y){
        
        // makeButtonが呼ばれたら、トーラスパズルは存在する
        doTorusExists();
        
        // (0, 0)に誤って振っても、追加できるようにする
        if(coord_x == 0 && coord_y == 0){
            Button newButton = new Button(-1, -1);
            // Torus.makeButton(XX, 0, 0)が入力された場合のケースをmButtonOnGridHorizontalに追加する
            mButtonOnGridHorizontal[coord_x] = newButton;
            
            
        // VERTICALとdirectionとして入力した場合
        }else if((coord_x == 0) && (coord_y >= 1 && coord_y <= MAX_BLOCK_FOR_VERTICAL)){
            Button newButton = new Button(direction, coord_y);
            mButtonOnGridHorizontal[coord_y] = newButton;
            
        // HORIZONTALとdirectionとして入力した場合
        }else if((coord_x >= 1 && coord_x <= MAX_BLOCK_FOR_HORIZONTAL) && (coord_y == 0)){
            Button newButton = new Button(direction, coord_x);
            mButtonOnGridVertical[coord_x] = newButton;
        }
    }
    
    /**
     * GUIに配列で管理しているデータを反映する関数
     */
    public void updateStage(){
        
        // 文字列出力をするときのタイトル
        Label lblText = new Label();
        lblText.setText("テキスト出力:");
        lblText.setFont(new Font(24));
        
        // TextAreaに文字列を表示する
        TextArea txtArea = new TextArea();
        txtArea.setText(mStringOutput.toString());
        txtArea.setEditable(false);
        txtArea.setFont(new Font(20));
        
        // テキストのサイズは関数の下部で実施
        
        // GridPaneレイアウトを使って、BlockOnGrid, ButtonOnGridを追加する
        GridPane torusGrid = new GridPane();
        torusGrid.setAlignment(Pos.CENTER);
        torusGrid.setPadding(new Insets(10, 10, 10, 10));
        torusGrid.setVgap(10);
        torusGrid.setHgap(10);
        // Blockを表示する処理
        for(int y = 0; y < MAX_BLOCK_FOR_VERTICAL; y++){
            for(int x = 0; x < MAX_BLOCK_FOR_HORIZONTAL; x++){
                
                // もし、Blockが特定の座標に登録されていない場合、GUIでは透明のButtonを配置
                if(mBlockOnGrid[y][x] == null){
                    javafx.scene.control.Button btn = new javafx.scene.control.Button();
                    // Blockのサイズをリサイズされても固定にするようにした。
                    // 固定するときは、Min, Pref, Maxに同じ値をセットすると固定される。
                    btn.setMinHeight(BUTTON_SIZE);
                    btn.setPrefHeight(BUTTON_SIZE);
                    btn.setMaxHeight(BUTTON_SIZE);
                    
                    btn.setMinWidth(BUTTON_SIZE);
                    btn.setPrefWidth(BUTTON_SIZE);
                    btn.setMaxWidth(BUTTON_SIZE);

                    GridPane.setConstraints(btn, x + 1, y + 1);
                    btn.setVisible(false);
                    continue;
                }
                
                int number = mBlockOnGrid[y][x].getNumber();
                javafx.scene.control.Button btn = new javafx.scene.control.Button(String.valueOf(number));
                // Blockのサイズをリサイズされても固定にするようにした。
                // 固定するときは、Min, Pref, Maxに同じ値をセットすると固定される。
                btn.setMinHeight(BUTTON_SIZE);
                btn.setPrefHeight(BUTTON_SIZE);
                btn.setMaxHeight(BUTTON_SIZE);

                btn.setMinWidth(BUTTON_SIZE);
                btn.setPrefWidth(BUTTON_SIZE);
                btn.setMaxWidth(BUTTON_SIZE);
                
                // Buttonの見た目を調整
                btn.setStyle("-fx-base: #33cc33");
                btn.setFont(new Font(24));
                GridPane.setConstraints(btn, x + 1, y + 1);
                torusGrid.getChildren().add(btn);
            }
        }
        
        //Torus.makeButton(XX, 0, 0)が入力されたときの処理
        if(mButtonOnGridHorizontal[0] != null){
            javafx.scene.control.Button btn = new javafx.scene.control.Button();
            // Buttonのサイズをリサイズされても固定にするようにした。
            // 固定するときは、Min, Pref, Maxに同じ値をセットすると固定される。
            btn.setMinHeight(BUTTON_SIZE);
            btn.setPrefHeight(BUTTON_SIZE);
            btn.setMaxHeight(BUTTON_SIZE);

            btn.setMinWidth(BUTTON_SIZE);
            btn.setPrefWidth(BUTTON_SIZE);
            btn.setMaxWidth(BUTTON_SIZE);
            
            // Buttonの見た目を調整
            btn.setStyle("-fx-base: #ff0000");
            
            GridPane.setConstraints(btn, 0, 0);
            torusGrid.getChildren().add(btn);
        }
        
        //ButtonのHorizontalを表示する処理
        for(int x = 1; x < MAX_BUTTON_FOR_HORIZONTAL; x++){
            
            // もし、Buttonが特定の座標に登録されていない場合、GUIでは透明のButtonを配置
            if(mButtonOnGridHorizontal[x] == null){
                javafx.scene.control.Button btn = new javafx.scene.control.Button();
                // Buttonのサイズをリサイズされても固定にするようにした。
                // 固定するときは、Min, Pref, Maxに同じ値をセットすると固定される。
                btn.setMinHeight(BUTTON_SIZE);
                btn.setPrefHeight(BUTTON_SIZE);
                btn.setMaxHeight(BUTTON_SIZE);

                btn.setMinWidth(BUTTON_SIZE);
                btn.setPrefWidth(BUTTON_SIZE);
                btn.setMaxWidth(BUTTON_SIZE);
                
                btn.setVisible(false);
                GridPane.setConstraints(btn, 0, x);
                torusGrid.getChildren().add(btn);
                continue;
            }
            
            javafx.scene.control.Button btn = new javafx.scene.control.Button();
            // Buttonのサイズをリサイズされても固定にするようにした。
            // 固定するときは、Min, Pref, Maxに同じ値をセットすると固定される。
            btn.setMinHeight(BUTTON_SIZE);
            btn.setPrefHeight(BUTTON_SIZE);
            btn.setMaxHeight(BUTTON_SIZE);

            btn.setMinWidth(BUTTON_SIZE);
            btn.setPrefWidth(BUTTON_SIZE);
            btn.setMaxWidth(BUTTON_SIZE);
            
            // Buttonのみたい目の調整
            btn.setStyle("-fx-base: #ff0000");
            // Buttonにイベントリスナーを付加
            // Buttonの配置場所がBlockに比べて一つずれているため、引いている
            EventHandler<ActionEvent> TorusEvent = null;
            if(mButtonOnGridHorizontal[x].getDirection() == Button.MOVE_HORIZONTAL){
                TorusEvent = new TorusEventHandlerHorizontal(this, x - 1);
            }else if(mButtonOnGridHorizontal[x].getDirection() == Button.MOVE_VERTICAL){
                TorusEvent = new TorusEventHandlerVertical(this, x - 1);
            }
            btn.setOnAction(TorusEvent);
            GridPane.setConstraints(btn, 0, x);
            torusGrid.getChildren().add(btn);
        }
        
        //ButtonのVerticalを表示する処理
        for(int y = 1; y < MAX_BUTTON_FOR_VERTICAL; y++){
            
            // もし、Buttonが特定の座標に登録されていない場合、GUIでは透明のButtonを配置
            if(mButtonOnGridVertical[y] == null){
                javafx.scene.control.Button btn = new javafx.scene.control.Button();
                // Buttonのサイズをリサイズされても固定にするようにした。
                // 固定するときは、Min, Pref, Maxに同じ値をセットすると固定される。
                btn.setMinHeight(BUTTON_SIZE);
                btn.setPrefHeight(BUTTON_SIZE);
                btn.setMaxHeight(BUTTON_SIZE);

                btn.setMinWidth(BUTTON_SIZE);
                btn.setPrefWidth(BUTTON_SIZE);
                btn.setMaxWidth(BUTTON_SIZE);
                
                btn.setVisible(false);
                GridPane.setConstraints(btn, y, 0);
                torusGrid.getChildren().add(btn);
                continue;
            }
            
            javafx.scene.control.Button btn = new javafx.scene.control.Button();
            // Buttonのサイズをリサイズされても固定にするようにした。
            // 固定するときは、Min, Pref, Maxに同じ値をセットすると固定される。
            btn.setMinHeight(BUTTON_SIZE);
            btn.setPrefHeight(BUTTON_SIZE);
            btn.setMaxHeight(BUTTON_SIZE);

            btn.setMinWidth(BUTTON_SIZE);
            btn.setPrefWidth(BUTTON_SIZE);
            btn.setMaxWidth(BUTTON_SIZE);
            
            // Buttonの見た目調整
            btn.setStyle("-fx-base: #ff0000");
            // Buttonにイベントリスナーを付加
            // Buttonの配置場所がBlockに比べて一つずれているため、引いている
            EventHandler<ActionEvent> TorusEvent = null;
            if(mButtonOnGridVertical[y].getDirection() == Button.MOVE_HORIZONTAL){
                TorusEvent = new TorusEventHandlerHorizontal(this, y - 1);
            }else if(mButtonOnGridVertical[y].getDirection() == Button.MOVE_VERTICAL){
                TorusEvent = new TorusEventHandlerVertical(this, y - 1);
            }
            btn.setOnAction(TorusEvent);
            GridPane.setConstraints(btn, y, 0);
            torusGrid.getChildren().add(btn);
        }        
        
        // 親レイアウトの宣言（この子としてtorusGridやtextAreaが含まれる）
        BorderPane layout = new BorderPane();

        // 文字列出力用にタイトル（lblText）、文字列出力内容（txtArea）をタテに表示するためのレイアウト
        VBox boxText = new VBox();

        // 条件: トーラスパズルが存在して AND 文字列出力する
        if(mTorusExist && mStringOutput.toString().length() > 0){
            layout.setCenter(torusGrid);
            
            // 適切なサイズを計算
            double PreferedSize = (double) TorusPuzzleGUI.GUI_WIDTH - (6 * (PREFERED_GRID_SIZE + 10));

            txtArea.setMinWidth(PreferedSize);
            txtArea.setPrefWidth(PreferedSize);
            txtArea.setMaxWidth(PreferedSize);

            txtArea.setPrefHeight(TorusPuzzleGUI.GUI_HEIGHT);
            
            boxText.getChildren().addAll(lblText, txtArea);
            layout.setLeft(boxText);
            
        // 条件: トーラスパズルのみ存在する
        }else if(mTorusExist && mStringOutput.toString().length() == 0){
            layout.setCenter(torusGrid);
        }
        // 条件: 文字列出力のみ存在する
        else if(!mTorusExist && mStringOutput.toString().length() >0){
            txtArea.setPrefSize(TorusPuzzleGUI.GUI_WIDTH, TorusPuzzleGUI.GUI_HEIGHT);
            boxText.getChildren().addAll(lblText, txtArea);
            layout.setLeft(boxText);
        }

        // 親レイアウトの見た目調整
        layout.setStyle("-fx-base: #ffffff");
        
        // レイアウトを決まったサイズに指定する
        Scene scene = new Scene(layout, TorusPuzzleGUI.GUI_WIDTH, TorusPuzzleGUI.GUI_HEIGHT);
        
        // レイアウトのデータをGUIに反映
        mGUI.setScene(scene);
        
    }
    
    /**
     * Blockを水平に動かすための関数
     * イベントハンドラーから呼び出される関数
     * @param index どの行のBlockを水平に動かすか(0はじまり)
     */
    public void moveHorizontally(int index){
        if(!(index >= 0 && index < MAX_BLOCK_FOR_VERTICAL)){
            return;
        }
        
        Block temporary = mBlockOnGrid[index][0];
        for(int i = 0; i < MAX_BLOCK_FOR_HORIZONTAL - 1; i++){
            mBlockOnGrid[index][i] = mBlockOnGrid[index][i + 1];
        }
        mBlockOnGrid[index][MAX_BLOCK_FOR_HORIZONTAL - 1] = temporary;
        
        // 画面を更新
        updateStage();
    }
    
    /**
     * Blockを垂直に動かすための関数
     * イベントハンドラーから呼び出される関数
     * @param index どの列のBlockを垂直に動かすか(0はじまり)
     */
    public void moveVertically(int index){
        if(!(index >= 0 && index < MAX_BLOCK_FOR_HORIZONTAL)){
            return;
        }
        
        Block temporary = mBlockOnGrid[0][index];
        for(int i = 0; i < MAX_BLOCK_FOR_VERTICAL - 1; i++){
            mBlockOnGrid[i][index] = mBlockOnGrid[i + 1][index];
        }
        mBlockOnGrid[MAX_BLOCK_FOR_VERTICAL - 1][index] = temporary;
        
        // 画面の更新
        updateStage();
    }
    
    /**
     * トーラスパズルが存在するかを報告を受ける関数
     */
    private void doTorusExists(){
        if(!mTorusExist){
            mTorusExist = true;
        }
    }
}
