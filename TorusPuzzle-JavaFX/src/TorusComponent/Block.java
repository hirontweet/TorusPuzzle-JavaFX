/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TorusComponent;

/**
 *
 * @author Yamazaki Laboratory
 * 
 * 緑のハコを表すクラス
 * GUIコンポーネントのButtonに数字を渡して表示を担当してもらう
 */
public class Block {
    
    private int mNumber;
    
    public Block(int number){
        mNumber = number;
    }
    
    public int getNumber(){
        return mNumber;
    }
}
