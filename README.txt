=== TorusPuzzle-JavaFXについての説明 ===

1. はじめに
TorusPuzzle-JavaFXは研究室で受け持つSSH、高校生訪問、高校生インストラクター、サイエンスインストラクターなどで使われていたFlash版のトーラスパズルをJavaFX(JavaのGUI)に移植したものになります。

移植した理由は２つあり、１つは環境構築の難しさ、２つ目はAdobeによるFlashの開発終了、バグなどFlashのプラットフォームによる問題です。

それに対して、移植されたJavaFXは、Java SE 7(7u8)以降のJDKをインストールするとJavaFXの実行環境が同梱されているため、環境に左右されず、32bit, 64bitでも実行ができます。


２． 環境の準備
TorusPuzzle-JavaFXは以下のような環境が必要です。

OS: Windows（Linuxはまだ検証していません）
Javaのバージョン: Java Development Kit SE (通称JDK) 7(7u8)以上、JDK SE 8でも可能
統合開発環境: Oracle NetBeans(32bit/64bit) Java SEのバージョン

また、Java特有の環境変数の設定などを済ませてください。


３．プロジェクトの使い方
TorusPuzzle-JavaFXを任意のディレクトリにコピーします。
NetBeansを開き、開いたら[ファイル] -> [プロジェクトを開く]を押して、コピーしたプロジェクトを選択します。
これでプロジェクトが使えるようになります。


４．トーラスパズルのプログラミング
「ソース・パッケージ」内にあるTorusUserというパッケージ内にUserCode.javaというソースがあります。
このソース内のmyCode()関数内にコードを書くと実行時の画面に表示がされます。